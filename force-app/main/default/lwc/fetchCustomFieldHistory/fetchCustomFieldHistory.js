import { LightningElement, track } from 'lwc';

import fetchFieldHistory from "@salesforce/apex/FieldHistoryController.fetchFieldHistory";

export default class FetchCustomFieldHistory extends LightningElement {

    @track showComp;
    @track dataObj;
    @track showSpinner;
    @track status = 'Active';

    connectedCallback(){
        this.showSpinner = true;
        fetchFieldHistory({
            status: this.status
            })
            .then(result => {
                if (result && result.length > 0) {
                    this.dataObj = result;
                    this.showComp = true;
                }else{
                    this.showComp = false;
                }
            })
            .catch(error => {
                console.log('error ' + error);
            })
            .finally(() => this.showSpinner = false);
    }
}