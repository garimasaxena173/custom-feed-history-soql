public with sharing class FieldHistoryController {
    @AuraEnabled
    public static List<fieldHistoryWrapper> fetchFieldHistory(String status) {
        try {
            List<Business_Information__History> historyRecords = getFieldHistoryFromBizInfo(status);
            List<fieldHistoryWrapper> resultList = new List<fieldHistoryWrapper>();
            for(Business_Information__History temp : historyRecords){
                fieldHistoryWrapper wrapper = new fieldHistoryWrapper();
                wrapper.oldValue = (String)temp.OldValue;
                wrapper.newValue = (String)temp.NewValue;
                DateTime dT = temp.CreatedDate;
                Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
                wrapper.createdDate = myDate;
                resultList.add(wrapper);
            }
            return resultList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    //Business_Information__c is a Custom Object
    public static List<Business_Information__History> getFieldHistoryFromBizInfo(String status){
        return [SELECT CreatedDate, OldValue, NewValue, Field 
                FROM Business_Information__History 
                WHERE Field = 'Business_Name__c' 
                AND Status__c =:status
                WITH SECURITY_ENFORCED]; 
    }

    public class fieldHistoryWrapper{
        @AuraEnabled public String oldValue;
        @AuraEnabled public String newValue;
        @AuraEnabled public Date createdDate;
    }
}
